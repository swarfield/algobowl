g++ -o main.out -g -std=c++11 -pthread *.cpp
rm outputs/results.txt
touch outputs/results.txt

for i in `seq 1 21`;
do
    echo "OUTPUT: $i"
    cat inputs/input_group$i.txt | ./main.out
    mv output.txt outputs/output$i.txt 

    # Write results to disk
    echo -n "OUTPUT: $i " >> outputs/results.txt; \
    python3 ../output/verifier.py inputs/input_group$i.txt outputs/output$i.txt  \
        >> outputs/results.txt; \
    echo "" >> outputs/results.txt &
done

# final data is out of order
echo "OUTPUT: 38"
    cat inputs/input_group38.txt | ./main.out
    mv output.txt outputs/output38.txt

    echo -n "OUTPUT: 38 " >> outputs/results.txt; \
    python3 ../output/verifier.py inputs/input_group38.txt outputs/output38.txt \
        >> outputs/results.txt; \
    echo "" >> outputs/results.txt &